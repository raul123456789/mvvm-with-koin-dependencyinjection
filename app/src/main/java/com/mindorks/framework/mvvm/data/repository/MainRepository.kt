package com.mindorks.framework.mvvm.data.repository

import com.mindorks.framework.mvvm.data.api.ApiHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainRepository (private val apiHelper: ApiHelper) {

    suspend fun getUsers() =  withContext(Dispatchers.IO) {
        apiHelper.getUsers()
    }

}